(function(window) {
    var svgSprite = "<svg>" + "" + '<symbol id="icon-bank" viewBox="0 0 1098 1024">' + "" + '<path d="M548.571429 0l548.571429 219.428571 0 73.142857-73.142857 0q0 14.848-11.702857 25.746286t-27.721143 10.825143l-872.009143 0q-16.018286 0-27.721143-10.825143t-11.702857-25.746286l-73.142857 0 0-73.142857zM146.285714 365.714286l146.285714 0 0 438.857143 73.142857 0 0-438.857143 146.285714 0 0 438.857143 73.142857 0 0-438.857143 146.285714 0 0 438.857143 73.142857 0 0-438.857143 146.285714 0 0 438.857143 33.718857 0q16.018286 0 27.721143 10.825143t11.702857 25.746286l0 36.571429-950.857143 0 0-36.571429q0-14.848 11.702857-25.746286t27.721143-10.825143l33.718857 0 0-438.857143zM1057.718857 914.285714q16.018286 0 27.721143 10.825143t11.702857 25.746286l0 73.142857-1097.142857 0 0-73.142857q0-14.848 11.702857-25.746286t27.721143-10.825143l1018.294857 0z"  ></path>' + "" + "</symbol>" + "" + '<symbol id="icon-location" viewBox="0 0 1024 1024">' + "" + '<path d="M486.61333299999984 11.349333000000001C493.0559999999999 4.138667000000169 502.3146669999998 1.1368683772161603e-13 511.9999999999999 1.1368683772161603e-13 521.7279999999998 0 530.9439999999998 4.138667000000055 537.4293329999998 11.349333000000115 576.7679999999998 55.25333300000011 921.6 445.09866699999986 921.6 619.7759999999998 921.6 802.8586669999997 738.9013330000002 1024 512.0000000000001 1024 406.22933300000017 1024 303.14666700000015 976.682667 221.73866700000013 890.8373330000001 147.02933300000012 811.9893330000002 102.40000000000009 710.6560000000002 102.39999999999992 619.7760000000002 102.39999999999998 445.0986670000001 447.2746669999998 55.25333300000011 486.61333299999984 11.349333000000001ZM512 955.733333C695.8506670000002 955.7333329999999 853.333333 771.029333 853.333333 619.7759999999998 853.333333 503.16799999999984 626.688 217.941333 511.9999999999999 85.75999999999999 397.35466699999984 217.94133300000004 170.66666699999996 503.2533330000002 170.66666700000002 619.776 170.66666700000002 771.029333 328.192 955.733333 512 955.733333ZM512 464.98133299999995C606.122667 464.98133299999995 682.666667 541.568 682.6666670000001 635.6479999999998 682.6666670000002 729.770667 606.1226670000001 806.3146669999999 512.0000000000002 806.3146669999999 417.9200000000001 806.3146670000001 341.33333300000004 729.770667 341.33333300000004 635.6480000000003 341.33333300000004 541.568 417.91999999999996 464.98133300000006 512 464.98133299999995ZM512.0000000000001 738.048C568.490667 738.048 614.4 692.1386669999999 614.4 635.648 614.4 579.1999999999998 568.4906669999999 533.2479999999999 512 533.248 455.552 533.248 409.6 579.2 409.6 635.648 409.6 692.1386669999999 455.5520000000001 738.0480000000001 512.0000000000001 738.048Z"  ></path>' + "" + "</symbol>" + "" + "</svg>";
    var script = function() {
        var scripts = document.getElementsByTagName("script"); 
        return scripts[scripts.length - 1]
    }();
    var shouldInjectCss = script.getAttribute("data-injectcss");
    var ready = function(fn) {
        if (document.addEventListener) {
            if (~["complete", "loaded", "interactive"].indexOf(document.readyState)) { setTimeout(fn, 0) } else {
                var loadFn = function() {
                    document.removeEventListener("DOMContentLoaded", loadFn, false);
                    fn()
                };
                document.addEventListener("DOMContentLoaded", loadFn, false)
            }
        } else if (document.attachEvent) { IEContentLoaded(window, fn) }

        function IEContentLoaded(w, fn) {
            var d = w.document,
                done = false,
                init = function() {
                    if (!done) {
                        done = true;
                        fn()
                    }
                };
            var polling = function() {
                try { d.documentElement.doScroll("left") } catch (e) { setTimeout(polling, 50); return }
                init()
            };
            polling();
            d.onreadystatechange = function() {
                if (d.readyState == "complete") {
                    d.onreadystatechange = null;
                    init()
                }
            }
        }
    };
    var before = function(el, target) { target.parentNode.insertBefore(el, target) };
    var prepend = function(el, target) { if (target.firstChild) { before(el, target.firstChild) } else { target.appendChild(el) } };

    function appendSvg() {
        var div, svg;
        div = document.createElement("div");
        div.innerHTML = svgSprite;
        svgSprite = null;
        svg = div.getElementsByTagName("svg")[0];
        if (svg) {
            svg.setAttribute("aria-hidden", "true");
            svg.style.position = "absolute";
            svg.style.width = 0;
            svg.style.height = 0;
            svg.style.overflow = "hidden";
            prepend(svg, document.body)
        }
    }
    if (shouldInjectCss && !window.__iconfont__svg__cssinject__) { window.__iconfont__svg__cssinject__ = true; try { document.write("<style>.svgfont {display: inline-block;width: 1em;height: 1em;fill: currentColor;vertical-align: -0.1em;font-size:16px;}</style>") } catch (e) { console && console.log(e) } }
    ready(appendSvg)
})(window)