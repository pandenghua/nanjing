! function() {
    var Arrow = {};
    Arrow.Create = function() {
        return Arrow_Create();
    }

    function Arrow_Create() {
        var _ = {};
        _._urlid = "arrow";
        _._path = "M0,-1.5 L1,0 L0,1.5";
        _._dom;
        _._line = d3.svg.line()
            .interpolate("basis");
        _.Scenes = function(dom) {
            _._scenes = d3.select(dom);
            return _;
        }
        _.Guanxi = function(s) {
            _.guanxi = s;
            return _;
        }
        _.Container = function(dom) {
            _._container = d3.select(dom)
            return _;
        }
        _.Data = function(data) {
            _.data = data;

            return _;
        }
        _.Draw = function() {
            function Arrow_Draw() {
                var defs = _._scenes
                    .append("defs")
                    .attr("class", "arrow_defs");

                var marker = defs
                    .append("marker")
                    .attr({
                        "id": _._urlid,
                        "markerUnits": "strokeWidth",
                        "viewBox": "0 -4 10 10",
                        "refX": 0,
                        "refY": 0,
                        "orient": "auto"
                    });

                var path = marker
                    .append("path")
                    .attr({
                        "class": "arrow_path",
                        "d": _._path,
                        "fill": "none",
                        "stroke": "white",
                        "stroke-width": ".2px"
                    })
            };
            _._scenes.selectAll(".arrow_defs").node() || Arrow_Draw();
            _.data_ = _.data.map(function(d) {
                return d.map(function(b) {
                    return b * _.guanxi
                })
            })
            var path = _._container.selectAll(".arrows_").data([_.data_]);
            path
                .enter()
                .insert("path", "use")
                // .append("path")
                // .insert("path")
                .attr({
                    "class": "arrows_",
                    "d": function(d) {
                        return _._line(d);
                    },
                    "marker-mid": function(d) {
                        return "url(#" + (_._urlid) + ")"
                    }
                })
                .style({
                    "stroke": "#00bcd4",
                    "stroke-width": "10px",
                    "fill": "none"
                })
            path
                .attr({
                    "d": function(d) {
                        return _._line(d);
                    }
                });
            path.exit().remove();

            _._dom = path;
            return _;

        }
        return _;
    }
    window.Arrow = Arrow;
}()