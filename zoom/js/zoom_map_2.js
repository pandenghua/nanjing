! function() {
    var zoom_map = {};
    console.log('jiazai')
    zoom_map.Create = function(str) {
        return map_create(str)
    }

    function map_create(str) {
        var _ = ObjInit();
        var box, dom, tool, img_data;
        box = _.box;
        box = getViewPort();
        getAspectRatiol(box)
        dom = _.dom;
        tool = _.tool;
        if (str) _.labelid = str;
        img_data = _.img_data;
        // var box = getViewPort();
        // getAspectRatiol(box);
        _.Labelid = function(str) {
            _.labelid = str;
            return _;
        }

        // 默认设置
        img_data = [2000, 1252, 'cad_2.jpg'];
        //设置导览地图
        _.SetImgMap = function(map) {
            _.img_data = map;
            img_data = _.img_data;
            getAspectRatiol(img_data)
            return _;
        }

        var backgroundImg = '#000';
        //设置背景图片url
        _.SetBackgroundImg = function(img) {
            _.backgroundImg = "url(' " + img + " ')";
            backgroundImg = _.backgroundImg;
            return _;
        }

        //添加标注点
        _.AddPrompt = function(prompt) {
            _.prompt = prompt;
            return _;
        }

        //绑定事件
        _.BindClickEvent = function(Fun) {
            _.bindClickEvent = Fun;
            return _;
        }

        _.brushWidth = 100;
        brushWidth = _.brushWidth;
        //brush-width
        _.SetBrushWidth = function(w) {
            _.brushWidth = w;
            brushWidth = _.brushWidth;
            return _;
        }

        var localCoordinate = [0, 0];
        //设置本人坐标；
        _.SetLocalCoordinate = function(coordinate) {
            _.localCoordinate = coordinate;
            localCoordinate = _.localCoordinate;
            return _;
        }

        _.SetPromptLogo = function(s) {
            _.promptLogo = s;
            return _;
        }

        _.SetLocalLogo = function(s) {
            _.localLogo = s;
            return _;
        }

        _.ShowPrompt = function() {

            return _;
        }

        _.ShowLocal = function() {

            return _;
        }

        _.Show = function Show() {
            Draw_()
            return _;
        }

        initTool();

        function Draw_() {

            initMode();
            tool_();
            draw_chart();

            //如果路径存在；
            if (dom.path_) {
                _.path_
                    .Guanxi(1 / img_data.global.scale * img_data.o.scale * _.transformModel.scale)
                    .Draw();
            }
        }

        function draw_chart() {
            draw_svg();
            draw_clip();
            draw_zoom_g();
            draw_global_g();
        }

        function draw_svg() {
            dom.svg = d3.selectAll("#" + _.labelid).selectAll("svg").data([img_data]);
            dom.svg.enter().append('svg');
            dom.svg.attr("width", box[0])
                .attr("height", box[1])
                .style("background", backgroundImg);
            dom.svg.exit().remove();

        }

        function draw_clip() {
            dom.clip = dom.svg.selectAll('#clip').data(['only']);
            dom.clip.enter()
                .append("defs")
                .append('clipPath')
                .attr("id", 'clip')
                .append("rect")
                .attr({
                    'width': box[0],
                    'height': box[1]
                });
            dom.clip.selectAll('rect')
                .attr('width', box[0])
                .attr('height', box[1])
            dom.clip.exit().remove()
        }

        function draw_zoom_g() {
            dom.zoom_g = dom.svg.selectAll(".zoom_g").data(['only'])
            dom.zoom_g.enter()
                .append("g")
                .attr({
                    "class": "zoom_g",
                    // "clip-path": "url(#clip)"
                })

            .call(tool.zoom);
            dom.zoom_g
                .attr({
                    "clip-path": "url(#clip)"
                })
                .call(tool.zoom);
            dom.zoom_g.exit().remove();

            //添加此层防抖动
            dom.zoom_gg = dom.zoom_g.selectAll(".zoom_gg").data(['only']);
            dom.zoom_gg.enter()
                .append('g')
                .attr({
                    "class": "zoom_gg"
                })
                .attr("transform", "translate(" + (_.transformModel.zoom[0]) + "," + (_.transformModel.zoom[1]) + ")")
            dom.zoom_gg.attr("class", "zoom_gg")
                .attr("transform", "translate(" + (_.transformModel.zoom[0]) + "," + (_.transformModel.zoom[1]) + ")")
                .on("click", function() {
                    var mouse_x = d3.event.clientX - _.transformModel.zoom[0];
                    var mouse_y = d3.event.clientY - _.transformModel.zoom[1];
                    tool.arrow = tool.arrow || Arrow.Create();
                    var data = [
                        [147, 630],
                        [98, 630],
                        [98, 807],
                        [423, 807],
                        [423, 863],
                        [62, 863],
                        [62, 1052],
                        [189, 1052],
                        [652, 800],
                        [652, 611]
                    ];
                    // var data_ = data.map(function(d) {
                    // return d.map(function(b) {
                    // return b / img_data.global.scale * img_data.o.scale * _.transformModel.scale;
                    // })
                    // })

                    //绘制路径
                    //bug 缩放正常，但是屏幕偏转bug。需重绘然后得 把绘制路径的dom赋值给zoom对象
                    var path_ = tool.arrow.Data(data)
                        .Guanxi(1 / img_data.global.scale * img_data.o.scale * _.transformModel.scale)
                        .Scenes(dom.svg.node())
                        .Container(dom.zoom_gg.node())
                        .Draw();
                    _.path_ = path_;
                    dom.path_ = path_._dom;

                })

            dom.zoom_gg.exit().remove();
            dom.zoom_img = dom.zoom_gg.selectAll("image").data([img_data]);
            dom.zoom_img
                .enter()
                .append("image")
                .attr({
                    "width": img_data.o.width,
                    "height": img_data.o.height,
                })
                .attr("xlink:href", function(d) {
                    return d[2];
                });
            dom.zoom_img
                .attr({
                    "width": img_data.o.width,
                    "height": img_data.o.height,
                })
                // .call(zoom_transform);
            dom.zoom_img.exit().remove();
            if (_.prompt) {
                dom.prompt = dom.zoom_gg.selectAll(".prompt").data(_.prompt);
                dom.prompt.enter()
                    .append("use")
                    .attr("class", "prompt")
                    .attr({
                        "width": "1em", //""+(20 /( img_data.zoom.scale * img_data.global.scale) )+"px",
                        "height": "1em" // ""+(30 / img_data.zoom.scale * img_data.global.scale )+"px"
                    })
                    .style({
                        "transform-origin": "50% 50%",
                        "fill": "#fff",
                        "transform": function(d) {

                            var translate = "translate(" + (-10 + d[0] * (img_data.zoom.scale / img_data.global.scale)) + "px," + (-18 + d[1] * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                            var rotate = "rotate(" + ( /*Math.random() * 360*/ d[2]) + "deg)";
                            var scale = "scale(" + (1 / (img_data.zoom.scale / img_data.global.scale)) + ")"
                            return translate /*+ rotate*/ + scale;
                            // return "translate(-10px,-18px)"+"rotate(0deg)"
                            // "+(Math.random()*360)+"
                        }
                    })
                    .attr('xlink:href', function(d) {
                        return d.tag
                    })
                    // .attr("xlink:href", "#" + _.promptid)
                    .on("click", function(d) {
                        sync_translate_model([d[0], d[1]]);
                        tool.zoom.translate(_.transformModel.zoom)
                        dom.zoom_gg
                            .transition()
                            .duration(2000)
                            .attr("transform", function(d) {
                                return "translate(" + (_.transformModel.zoom[0]) + "," + (_.transformModel.zoom[1]) + ")" + "scale(" + (_.transformModel.scale) + ")"
                                    //  return "translate(" + (_.transformModel.zoom[0] /img_data.global.scale * img_data.zoom.scale) + "," + (_.transformModel.zoom[1] /img_data.global.scale * img_data.zoom.scale) + ")" + "scale(" + (img_data.zoom.scale / img_data.o.scale) + ")"
                            })
                        tool.brush.extent(_.transformModel.brush)
                        dom.global_brush.call(tool.brush)
                        dom.global_brush.exit().remove();
                        dom.global_brush.selectAll(".resize").remove();
                        dom.global_brush.selectAll(".background").remove();
                        _.click_event.call(_, d)
                    })
                dom.prompt
                    .style({
                        "transform": function(d) {
                            var translate = "translate(" + (-10 + d[0] * (img_data.zoom.scale / img_data.global.scale)) + "px," + (-18 + d[1] * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                            var rotate = "rotate(" + ( /*Math.random() * 360*/ d[2]) + "deg)";
                            var scale = "scale(" + (1 / (img_data.zoom.scale / img_data.global.scale)) + ")"

                            return translate /*+ rotate*/ + scale;
                            // return "translate(-10px,-18px)"+"rotate(0deg)"
                            // "+(Math.random()*360)+"
                        }
                    })
                dom.prompt.exit().remove()
            }

            dom.tag = dom.zoom_gg.selectAll(".tag").data([_.localCoordinate]);
            dom.tag.enter()
                .append("use")
                .attr("class", "tag")
                .attr({
                    "width": "1em", //""+(20 /( img_data.zoom.scale * img_data.global.scale) )+"px",
                    "height": "1em" // ""+(30 / img_data.zoom.scale * img_data.global.scale )+"px"
                })
                .style({
                    "transform-origin": "50% 68%",
                    "fill": "#fff",
                    "transform": function(d) {

                        var translate = "translate(" + (-10 + d[0] * (img_data.zoom.scale / img_data.global.scale)) + "px," + (-18 + d[1] * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + ( /*Math.random() * 360*/ d[2]) + "deg)";
                        var scale = "scale(" + (1 / (img_data.zoom.scale / img_data.global.scale)) + ")"
                        return translate + rotate + scale;
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                    }
                })
                .attr("xlink:href", "#" + _.tagid)
                // .on("click",_.click_event)
            dom.tag
                .style({
                    "transform": function(d) {
                        var translate = "translate(" + (-10 + d[0] * (img_data.zoom.scale / img_data.global.scale)) + "px," + (-18 + d[1] * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + ( /*Math.random() * 360*/ d[2]) + "deg)";
                        var scale = "scale(" + (1 / (img_data.zoom.scale / img_data.global.scale)) + ")"

                        return translate + rotate + scale;
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                    }
                })
            dom.tag.exit().remove()


        }

        function draw_global_g() {
            dom.global_g = dom.svg.selectAll(".global_g").data(['only'])
            dom.global_g.enter()
                .append("g")
                .style("opacity", .25)
                .attr("class", "global_g")

            ;
            dom.global_g /*.call(tool.brush)*/ ;
            dom.global_g.exit().remove();

            // dom.global_rect = dom.global_g.selectAll(".bg").data(['only'])
            // dom.global_rect.enter()
            //     .append("rect")
            //     .attr({
            //      "class":"bg",
            //         "width": img_data.global.width,
            //         "height": img_data.global.height,
            //     })
            //     .style("fill","none")
            //     .style("stroke", "#fff")
            //     .style("stroke-width", "2px");
            // dom.global_rect
            //  .attr({
            //      "width": img_data.global.width,
            //         "height": img_data.global.height,
            //  });
            // dom.global_rect.exit().remove();

            dom.global_img = dom.global_g.selectAll("image").data([img_data])
            dom.global_img.enter()
                .append("image")
                .attr("xlink:href", function(d) {
                    return d[2];
                })
            dom.global_img.call(global_transform);
            dom.global_img.exit().remove();
            dom.global_brush = dom.global_g.selectAll("._brush").data(['only'])
            dom.global_brush.enter()
                .append("g")
                .attr("class", "_brush")
                .call(tool.brush)
            dom.global_brush.call(tool.brush)
            dom.global_brush.exit().remove();
            dom.global_brush.selectAll(".resize").remove();
            dom.global_brush.selectAll(".background").remove();

            // dom.global_g.call(tool.brush);
            // dom.global_g.selectAll("g").remove();
            // dom.global_g.selectAll(".background").remove();
            // dom.global_g.selectAll(".resize").remove();
            // dom.global_g.selectAll("image").style("stroke","#fff");

        }

        function tool_() {
            tool.zoom.center(null)
                .scaleExtent([1, 5])
                .translate(_.transformModel.zoom)
                .on('zoomstart', function() {})
                .on('zoomend', function() {})
                .on('zoom', zoomed)
            var scale_x = d3.scale.linear()
                .domain([0, img_data.global.width])
                .rangeRound([0, img_data.global.width])
            var scale_y = d3.scale.linear()
                .domain([0, img_data.global.height])
                .rangeRound([0, img_data.global.height])
            tool.brush = d3.svg.brush()
                .x(scale_x)
                .y(scale_y)
                .on('brushstart', function() {
                    _.brush_extent = tool.brush.extent();
                    dom.global_g.style("opacity", 1);
                })
                .on('brush', brushed)
                .on('brushend', function() {
                    dom.global_g.style("opacity", .25)
                        .extent(_.transformModel.brush)
                })
        }

        function brushed() {

            var translate_ = tool.brush ? (
                [tool.brush.extent()[0][0], tool.brush.extent()[0][1]]
            ) : [0, 0];
            // if (tool.brush) {

            //  if (tool.brush.extent()[0][0] == _.brush_extent[0][0] && tool.brush.extent()[0][1] == _.brush_extent[0][1]) {
            //      tool.brush.extent(_.brush_extent);
            //      dom.global_brush.call(tool.brush)
            //  }
            // }

            // 同步transform记录
            _.transformModel.brush = tool.brush.extent();
            _.transformModel.zoom = translate_;
            _.transformModel.scale = _.transformModel.old.scale;
            // sync_translate_model("brush")
            syncTranslateModel('brush')
            dom.zoom_gg.attr("transform", function(d) {
                return "translate(-" + (translate_[0] * img_data.zoom.scale) + ",-" + (translate_[1] * img_data.zoom.scale) + ")" + "scale(" + (img_data.zoom.scale / img_data.o.scale) + ")"
            })
            tool.zoom.translate([-(translate_[0] * img_data.zoom.scale), -(translate_[1] * img_data.zoom.scale)])
            dom.zoom_g.call(tool.zoom)
                // d3.event.stopPropagation()

        }
        _.zoom_last = {
            scale: 1,
            translate: [0, 0]
        };
        var zoom_last = _.zoom_last;

        function zoomed() {

            dom.global_brush.call(tool.brush)
            dom.global_brush.exit().remove();
            dom.global_brush.selectAll(".resize").remove();
            dom.global_brush.selectAll(".background").remove();
            zoom_last.scale = d3.event.scale
            d3.selectAll("use")
                // .transition()
                .style("transform", function(d) {
                    var translate = "translate(" + (-10 + d[0] * (img_data.o.scale / img_data.global.scale)) + "px," + (-18 + d[1] * (img_data.o.scale / img_data.global.scale)) + "px)";
                    var rotate = "rotate(" + ( /*Math.random() * 360*/ d[2]) + "deg)";
                    var scale = "scale(" + (1 / d3.event.scale) + ")" //"scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                    return translate + rotate + scale;
                })
            var g_box = dom.zoom_gg.node().getBBox();
            //scale 无法缩放 或 平移
            if (d3.event.scale == zoom_last.scale) {
                //left
                if (d3.event.translate[0] > 0) {
                    d3.event.translate[0] = 0;
                }
                //top
                if (d3.event.translate[1] > 0) {
                    d3.event.translate[1] = 0;
                }

                //right
                if (Math.abs(g_box.width * d3.event.scale + d3.event.translate[0]) < box[0]) {
                    d3.event.translate[0] = box[0] - g_box.width * d3.event.scale;
                }

                //bottom
                if (Math.abs(g_box.height * d3.event.scale + d3.event.translate[1]) < box[1]) {
                    d3.event.translate[1] = box[1] - g_box.height * d3.event.scale;
                }

                tool.zoom.translate(d3.event.translate)
            }
            //缩小
            if (d3.event.scale < zoom_last.scale) {
                //left
                if (d3.event.translate[0] > 0) {
                    d3.event.translate[0] = 0;
                }
                //top
                if (d3.event.translate[1] > 0) {
                    d3.event.translate[1] = 0;
                }

                //right
                if (Math.abs(g_box.width * d3.event.scale + d3.event.translate[0]) < box[0]) {
                    d3.event.translate[0] = box[0] - g_box.width * d3.event.scale;
                }

                //bottom
                if (Math.abs(g_box.height * d3.event.scale + d3.event.translate[1]) < box[1]) {
                    d3.event.translate[1] = box[1] - g_box.height * d3.event.scale;
                }

                tool.zoom.translate(d3.event.translate)
            }
            //放大
            if (d3.event.scale > zoom_last.scale) {

            }
            if (img_data.factor >= box.factor) {
                img_data.zoom.height = img_data.global.height * d3.event.scale;
                img_data.zoom.width = img_data.global.width * d3.event.scale;
                img_data.zoom.scale = img_data.zoom.height / img_data.global.height
            } else {
                img_data.zoom.width = img_data.o.width * d3.event.scale;
                img_data.zoom.height = img_data.o.height * d3.event.scale;
                img_data.zoom.scale = img_data.zoom.height / img_data.global.height
            }
            dom.zoom_gg.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");




            var x_0 = Math.abs(d3.event.translate[0]) / img_data.zoom.scale;
            var y_0 = Math.abs(d3.event.translate[1]) / img_data.zoom.scale;
            var x_1 = (Math.abs(d3.event.translate[0]) + box[0]) / img_data.zoom.scale;
            var y_1 = (Math.abs(d3.event.translate[1]) + box[1]) / img_data.zoom.scale;
            tool.brush.extent([
                [x_0, y_0],
                [x_1, y_1]
            ])

            // 同步transform记录
            _.transformModel.brush = [
                [x_0, y_0],
                [x_1, y_1]
            ];
            _.transformModel.zoom = d3.event.translate;
            _.transformModel.scale = d3.event.scale;
            // sync_translate_model("zoom")
            syncTranslateModel('zoom')
                // brushed()
                // d3.event.stopPropagation()

        }
        // initMode()

        function initMode() {
            CreateScaleModel()
            CreateTranslateModel()
            syncTranslateModel(_.transformModel)
        }

        function initTool() {
            tool.zoom = d3.behavior.zoom();
            tool.brush = d3.svg.brush();
        }

        //初始化对象原型
        function ObjInit() {
            var obj = {};
            obj.labelid = '';
            obj.dom = {};
            obj.tool = {};
            obj.box = [];
            obj.des = '导览地图';
            return obj;
        }

        function CreateScaleModel() {

            //es6
            // img_data = [...img_data];
            // img_data = img_data.map(obj => obj);

            //es5 重置imgdata
            // img_data = img_data.map(function(obj) {
            // return obj;
            // })
            img_data = CopyArr(img_data);
            img_data = getAspectRatiol(img_data);

            img_data.global = {};
            img_data.global.width = brushWidth;
            img_data.global.height = brushWidth / img_data.aspectRatiol;
            img_data.global.scale = img_data[0] / img_data.global.width;

            img_data.zoom = {};
            if (img_data.aspectRatiol >= box.aspectRatiol) {
                img_data.zoom.height = box[1];
                img_data.zoom.width = box[1] * img_data.aspectRatiol;
            } else {
                img_data.zoom.width = box[0];
                img_data.zoom.height = box[0] * img_data.aspectRatiol;
            }
            img_data.zoom.scale = img_data.height / img_data.global.height;

            img_data.o = {
                width: img_data.zoom.width,
                height: img_data.zoom.height,
                scale: img_data.zoom.width / img_data.global.width
            };
        }

        function global_transform() {
            this.attr({
                height: function(d) {
                    return d.global.height
                },
                width: function(d) {
                    return d.global.width
                }
            })
            dom.global_g.attr("transform", function() {
                return "translate(0," + (box[1] - img_data.global.height) + ")"
            })
        }

        function CreateTranslateModel() {
            _.transformModel = {
                "brush": [
                    [0, 0],
                    [box[0] / img_data.o.scale, box[1] / img_data.o.scale]
                ],
                "zoom": [0, 0],
                "scale": 1
            }

            _.transformModel.init = {
                "brush": [
                    [0, 0],
                    [box[0] / img_data.o.scale, box[1] / img_data.o.scale]
                ],
                "zoom": [0, 0],
                "scale": 1
            };

            _.transformModel.old = {
                "brush": [
                    [0, 0],
                    [box[0] / img_data.o.scale, box[1] / img_data.o.scale]
                ],
                "zoom": [0, 0],
                "scale": 1
            }
        }

        function syncTranslateModel(tag) {
            var obj = {
                "brush": FunSyncOld,
                "zoom": FunSyncOld,
                "point": FunPoint
            }

            if (!obj[tag]) {
                // _.transformModel.old = {};
                var point = tag;
                tag = 'point';
            }
            obj[tag]();

            function FunSyncOld() {
                _.transformModel.old = {};
                deepCopy(_.transformModel, _.transformModel.old);
            }

            function FunPoint() {
                function transformBrushIndex(index) {
                    var indexPositionInGlobar = point[index] / img_data.global.scale;
                    var brush_index_abs = _.transformModel.brush[1][index] - _.transformModel.brush[0][index];
                    var img_index = index == 0 ? "width" : "height"
                    if (indexPositionInGlobar <= brush_index_abs / 2) {
                        _.transformModel.brush[0][index] = 0;
                        _.transformModel.brush[1][index] = brush_index_abs;
                    } else if (indexPositionInGlobar < img_data.global[img_index] - brush_index_abs / 2) {
                        _.transformModel.brush[0][index] = indexPositionInGlobar - brush_index_abs / 2;
                        _.transformModel.brush[1][index] = indexPositionInGlobar + brush_index_abs / 2;
                    } else {
                        _.transformModel.brush[0][index] = img_data.global[img_index] - brush_index_abs;
                        _.transformModel.brush[1][index] = img_data.global[img_index];
                    }
                    transform_brush_index(0);
                    transform_brush_index(1);
                    _.transformModel.zoom[0] = -_.transformModel.brush[0][0] * img_data.o.scale * _.transformModel.scale;
                    _.transformModel.zoom[1] = -_.transformModel.brush[0][1] * img_data.o.scale * _.transformModel.scale;
                    deepCopy(_.transformModel, _.transformModel.old)

                }
            }
        }

        function getAspectRatiol(view) {
            var arr = view //CopyArr(view);
            arr.aspectRatiol = arr[0] / arr[1];
            return arr;
        }

        function CopyArr(arr) {
            var arr_copy;
            arr_copy = arr.map(function(d) {
                return d;
            })
            return arr_copy;
        }

        function deepCopy(source, target) {
            var keys = ['brush', 'zoom', 'scale'];
            for (var i = 0; i < keys.length; i++) {
                target[keys[i]] = source[keys[i]]
            }
        }

        function getViewPort() {
            var horizontal = $(window).width();
            var vertical = $(window).height();
            return [horizontal, vertical];
        }
        // setTimeout(function() {
        window.addEventListener("deviceorientation", orientationHandler, false);
        // }, 1000)
        window.addEventListener('orientationchange', function() {
            // _.Draw()
            // dom.zoom_g.call(tool.zoom)

            _.Show()
                // _.zoomed()

        })

        function orientationHandler(event) {
            // _.Update()
            d3.selectAll('use')
                .style({
                    "transform": function(d) {
                        if (d3.select(this).attr("class") == "tag") {
                            d[2] = event.webkitCompassHeading || 180 - event.alpha;
                        } else {
                            d[2] = 0
                        }


                        // var anzhuo = 180//event.webkitCompassHeading ? 180 : 0
                        // d[2] = d3.select(this).attr("class") == "tag" ?( event.webkitCompassHeading || 180 - event.alpha) : anzhuo;
                        // return "translate(" + (d[0] - 10 * img_data.zoom.scale) + "px," + (d[1] - 18 * img_data.zoom.scale) + "px)" + "rotate(" + (d[2]) + "deg)"
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                        var translate = "translate(" + (-10 + d[0] * (img_data.o.scale / img_data.global.scale)) + "px," + (-18 + d[1] * (img_data.o.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (d[2]) + "deg)";
                        var scale = "scale(" + (1 / (img_data.zoom.scale / img_data.global.scale)) + ")"

                        return translate + rotate + scale;
                    }
                })
        }

        console.log(_)
        return _;
    }
    // return zoom_map;
    this.zoom_map = zoom_map;
}()