! function() {
    var zoom_map = {};
    zoom_map.create = function(i) {
        return map_create(i);
    }

    function map_create(i) {
        var _ = {};
        _.des = "缩放地图";
        if (i) _.labelid = i;
        _.box = [$(window).width(), $(window).height()];
        var box = _.box;
        _.dom = {};
        var dom = _.dom;
        _.tool = {};
        var tool = _.tool;
        box.aspect_ratiol = box[0] / box[1];

        _.transform = {}
        var img_data = [2000, 1252, "cad_2.jpg"];
        _.Img = function(arr) {
            _.img_data = arr;
            img_data = _.img_data;
            return _;
        }
        _.Prompt = function(a) {
            _.prompt = a;
            return _;
        }

        _.Click_event = function(Fun) {
            _.click_event = Fun;
            return _;
        }
        var Map_width = 100;
        _.Map_width = function(w) {
            Map_width = w;
            return _;
        }
        var background_img = "#000";
        _.BackgroundImg = function(s) {
            background_img = "url('" + s + "')";
            return _;
        }
        _.Draw = function() {
            return Draw_();
        }
        _.Tag_id = function(s) {
            _.tagid = s;
            return _;
        }
        _.Prompt_id = function(s) {
            _.promptid = s;
            return _;
        }
        _.Tag_coordinate = function(arr) {
            _.tagCoordinate = arr;
            return _;
        }

        function Draw_() {
            data_();
            tool_();
            chart_();
            return _;
        }
        _.Update = function() {
            return Update_();
        }

        function Update_() {
            _.box = [$(window).width(), $(window).height()];
            box = _.box;
            box.aspect_ratiol = box[0] / box[1];

            data_();
            tool_();
            chart_();
            return _;
        }

        function chart_() {
            svg_();
            clip_();
            zoom_g_();
            global_g_();

        }

        function tool_() {
            
            tool.zoom = d3.behavior.zoom()
                .center(null)
                .scaleExtent([1, 5])
                .translate(_.transform.really.translate.zoom)
                .on("zoomstart", function() {
                    d3.select("use")
                    .style("transform",function(d) {
                        var translate = "translate(" + (- 10 +d[0]  * (img_data.o.scale / img_data.global.scale)) + "px," + (- 18 + d[1]  * (img_data.o.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (/*Math.random() * 360*/d[2]) + "deg)";
                        var scale = "scale("+(1 / d3.event.scale)+")" //"scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        return translate + rotate + scale;
                    })
                    // zoom.center(/*d3.event.translate*/null)
                })
                .on("zoomend", function() {})
                .on("zoom", zoomed);

            var scale_x = d3.scale.linear()
                .domain([0, img_data.global.width])
                .rangeRound([0, img_data.global.width])

            var scale_y = d3.scale.linear()
                .domain([0, img_data.global.height])
                .rangeRound([0, img_data.global.height])


            tool.brush = d3.svg.brush()
                .x(scale_x)
                .y(scale_y)
                .on("brushstart", function() {
                    _.brush_extent = tool.brush.extent();
                })
                .on("brush", brushed)
                .on("brushend", function() {

                })
                .extent(_.transform.really.translate.brush)
        }

        function brushed() {

            var translate_ = tool.brush ? (
                [tool.brush.extent()[0][0], tool.brush.extent()[0][1]]
            ) : [0, 0];
            // if (tool.brush) {

            //  if (tool.brush.extent()[0][0] == _.brush_extent[0][0] && tool.brush.extent()[0][1] == _.brush_extent[0][1]) {
            //      tool.brush.extent(_.brush_extent);
            //      dom.global_brush.call(tool.brush)
            //  }
            // }

            // 同步transform记录
            _.transform.really.brush = tool.brush.extent();
            _.transform.really.zoom = translate_;
            _.transform.cu = _.transform.really;

            dom.zoom_gg.attr("transform", function(d) {
                return "translate(-" + (translate_[0] * img_data.zoom.scale) + ",-" + (translate_[1] * img_data.zoom.scale) + ")" + "scale(" + (img_data.zoom.scale / img_data.o.scale) + ")"
            })
            tool.zoom.translate([-(translate_[0] * img_data.zoom.scale),-(translate_[1] * img_data.zoom.scale)])
            dom.zoom_g.call(tool.zoom)
            d3.event.stopPropagation()
        }
        _.zoom_last = {
            scale: 1,
            translate: [0, 0]
        };
        var zoom_last = _.zoom_last;

        function zoomed() {

             dom.global_brush.call(tool.brush)
            dom.global_brush.exit().remove();
            dom.global_brush.selectAll(".resize").remove();
            dom.global_brush.selectAll(".background").remove();
            zoom_last.scale = d3.event.scale
                d3.select("use")
                // .transition()
                    .style("transform",function(d) {
                        var translate = "translate(" + (- 10 +d[0]  * (img_data.o.scale / img_data.global.scale)) + "px," + (- 18 + d[1]  * (img_data.o.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (/*Math.random() * 360*/d[2]) + "deg)";
                        var scale = "scale("+(1 / d3.event.scale)+")" //"scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        return translate + rotate + scale;
                    })
            var g_box = dom.zoom_gg.node().getBBox();
            //scale 无法缩放 或 平移
            if (d3.event.scale == zoom_last.scale) {
                //left
                if (d3.event.translate[0] > 0) {
                    d3.event.translate[0] = 0;
                }
                //top
                if (d3.event.translate[1] > 0) {
                    d3.event.translate[1] = 0;
                }

                //right
                if (Math.abs(g_box.width * d3.event.scale + d3.event.translate[0]) < box[0]) {
                    d3.event.translate[0] = box[0] - g_box.width * d3.event.scale;
                }

                //bottom
                if (Math.abs(g_box.height * d3.event.scale + d3.event.translate[1]) < box[1]) {
                    d3.event.translate[1] = box[1] - g_box.height * d3.event.scale;
                }

                tool.zoom.translate(d3.event.translate)
            }
            //缩小
            if (d3.event.scale < zoom_last.scale) {
                //left
                if (d3.event.translate[0] > 0) {
                    d3.event.translate[0] = 0;
                }
                //top
                if (d3.event.translate[1] > 0) {
                    d3.event.translate[1] = 0;
                }

                //right
                if (Math.abs(g_box.width * d3.event.scale + d3.event.translate[0]) < box[0]) {
                    d3.event.translate[0] = box[0] - g_box.width * d3.event.scale;
                }

                //bottom
                if (Math.abs(g_box.height * d3.event.scale + d3.event.translate[1]) < box[1]) {
                    d3.event.translate[1] = box[1] - g_box.height * d3.event.scale;
                }

                tool.zoom.translate(d3.event.translate)
            }
            //放大
            if (d3.event.scale > zoom_last.scale) {

            }
            if (img_data.factor >= box.factor) {
                img_data.zoom.height = img_data.global.height * d3.event.scale;
                img_data.zoom.width = img_data.global.width * d3.event.scale;
                img_data.zoom.scale = img_data.zoom.height / img_data.global.height
            } else {
                img_data.zoom.width = img_data.o.width * d3.event.scale;
                img_data.zoom.height = img_data.o.height * d3.event.scale;
                img_data.zoom.scale = img_data.zoom.height / img_data.global.height
            }
            dom.zoom_gg.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");




            var x_0 = Math.abs(d3.event.translate[0]) / img_data.zoom.scale;
            var y_0 = Math.abs(d3.event.translate[1]) / img_data.zoom.scale;
            var x_1 = (Math.abs(d3.event.translate[0]) + box[0]) / img_data.zoom.scale;
            var y_1 = (Math.abs(d3.event.translate[1]) + box[1]) / img_data.zoom.scale;
            tool.brush.extent([
                    [x_0, y_0],
                    [x_1, y_1]
                ])

            // 同步transform记录
            _.transform.really.brush = [
                    [x_0, y_0],
                    [x_1, y_1]
                ];
            _.transform.really.zoom = d3.event.translate;
            _.transform.really.scale = d3.event.scale;
            _.transform.cu = _.transform.really;
                // brushed()
           d3.event.stopPropagation()
        }

        function svg_() {
            dom.svg = d3.selectAll("#" + _.labelid).selectAll("svg").data([img_data]);
            dom.svg.enter().append("svg");
            dom.svg.attr("width", box[0])
                .attr("height", box[1])
                .style("background", background_img);
            dom.svg.exit().remove();

        }

        function clip_() {
            dom.clip = dom.svg.selectAll("#clip").data(['only'])
            dom.clip.enter()
                .append("defs")
                .append("clipPath")
                .attr({
                    "id": "clip"
                })
                .append("rect")
                .attr({
                    "width": box[0],
                    "height": box[1]
                });
            dom.clip.selectAll("rect")
                .attr({
                    "width": box[0],
                    "height": box[1]
                });
            dom.clip.exit().remove();

        }

        function zoom_g_() {
            dom.zoom_g = dom.svg.selectAll(".zoom_g").data(['only'])
            dom.zoom_g.enter()
                .append("g")
                .attr({
                    "class": "zoom_g",
                    // "clip-path": "url(#clip)"
                })

                .call(tool.zoom);
            dom.zoom_g
                .attr({
                    "clip-path": "url(#clip)"
                })
                .call(tool.zoom);
            dom.zoom_g.exit().remove();

            //添加此层防抖动
            dom.zoom_gg = dom.zoom_g.selectAll(".zoom_gg").data(['only']);
            dom.zoom_gg.enter()
                .append('g')
                .attr({
                    "class": "zoom_gg"
                })
                .attr("transform","translate("+(_.transform.really.translate.zoom[0])+","+(_.transform.really.translate.zoom[1])+")") 
                
            dom.zoom_gg.attr("class", "zoom_gg")
            .on("click",function(){
                    console.log(d3.event,_.transform);
                    var mouse_x = d3.event.clientX - _.transform.really.zoom[0];
                    var mouse_y = d3.event.clientY - _.transform.really.zoom[1];
                    console.log(mouse_x,mouse_y) 

                })
            dom.zoom_gg.exit().remove();
            dom.zoom_img = dom.zoom_gg.selectAll("image").data([img_data]);
            dom.zoom_img
                .enter()
                .append("image")
                .attr({
                    "width": img_data.o.width,
                    "height": img_data.o.height,
                })
                .attr("xlink:href", function(d) {
                    return d[2];
                });
            dom.zoom_img
                .attr({
                    "width": img_data.o.width,
                    "height": img_data.o.height,
                })
                // .call(zoom_transform);
            dom.zoom_img.exit().remove();
            if (_.prompt) {
                dom.prompt = dom.zoom_gg.selectAll(".prompt").data(_.prompt);
            dom.prompt.enter()
                .append("use")
                .attr("class","prompt")
                .attr({
                    "width": "1em",//""+(20 /( img_data.zoom.scale * img_data.global.scale) )+"px",
                    "height":"1em" // ""+(30 / img_data.zoom.scale * img_data.global.scale )+"px"
                })
                .style({
                    "transform-origin": "50% 100%",
                    "fill": "#fff",
                    "transform": function(d) {
                        
                        var translate = "translate(" + (-10 + d[0]  *(img_data.zoom.scale / img_data.global.scale)) + "px," + ( - 18 + d[1]  * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (/*Math.random() * 360*/d[2]) + "deg)";
                        var scale = "scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        return translate /*+ rotate*/ + scale;
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                    }
                })
                .attr("xlink:href", "#" + _.promptid)
                .on("click",function(d) {
                    // console.log(d)
                    
                    zoom_translate_([d[0],d[1]]);
                    console.log(_.transform.really.translate.zoom)
                    tool.zoom.translate(_.transform.really.translate.zoom)
                     dom.zoom_gg.transition().duration(2000).attr("transform", function(d) {
                return "translate(" + (_.transform.really.translate.zoom[0] /img_data.global.scale * img_data.zoom.scale) + "," + (_.transform.really.translate.zoom[1] /img_data.global.scale * img_data.zoom.scale) + ")" + "scale(" + (img_data.zoom.scale / img_data.o.scale) + ")"
            })
                    tool.brush.extent(_.transform.really.translate.brush)
                         dom.global_brush.call(tool.brush)
                        dom.global_brush.exit().remove();
                        dom.global_brush.selectAll(".resize").remove();
                        dom.global_brush.selectAll(".background").remove();
                    _.click_event.call(_,d)
                })
               dom.prompt
                .style({
                    "transform": function(d) {
                        var translate = "translate(" + (-10 + d[0]  *(img_data.zoom.scale / img_data.global.scale)) + "px," + ( - 18 + d[1]  * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (/*Math.random() * 360*/d[2]) + "deg)";
                        var scale = "scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        
                        return translate /*+ rotate*/ + scale;
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                    }
                })
                dom.prompt.exit().remove()
            }

            dom.tag = dom.zoom_gg.selectAll(".tag").data([_.tagCoordinate]);
            dom.tag.enter()
                .append("use")
                .attr("class","tag")
                .attr({
                    "width": "1em",//""+(20 /( img_data.zoom.scale * img_data.global.scale) )+"px",
                    "height":"1em" // ""+(30 / img_data.zoom.scale * img_data.global.scale )+"px"
                })
                .style({
                    "transform-origin": "50% 68%",
                    "fill": "#fff",
                    "transform": function(d) {
                        
                        var translate = "translate(" + (-10 + d[0]  *(img_data.zoom.scale / img_data.global.scale)) + "px," + ( - 18 + d[1]  * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (/*Math.random() * 360*/d[2]) + "deg)";
                        var scale = "scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        return translate + rotate + scale;
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                    }
                })
                .attr("xlink:href", "#" + _.tagid)
                // .on("click",_.click_event)
               dom.tag
                .style({
                    "transform": function(d) {
                        var translate = "translate(" + (-10 + d[0]  *(img_data.zoom.scale / img_data.global.scale)) + "px," + ( - 18 + d[1]  * (img_data.zoom.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (/*Math.random() * 360*/d[2]) + "deg)";
                        var scale = "scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        
                        return translate + rotate + scale;
                        // return "translate(-10px,-18px)"+"rotate(0deg)"
                        // "+(Math.random()*360)+"
                    }
                })
                dom.tag.exit().remove()
                
        }

        function global_g_() {
            dom.global_g = dom.svg.selectAll(".global_g").data(['only'])
            dom.global_g.enter()
                .append("g")
                .attr("class", "global_g");
            dom.global_g /*.call(tool.brush)*/ ;
            dom.global_g.exit().remove();

            // dom.global_rect = dom.global_g.selectAll(".bg").data(['only'])
            // dom.global_rect.enter()
            //     .append("rect")
            //     .attr({
            //      "class":"bg",
            //         "width": img_data.global.width,
            //         "height": img_data.global.height,
            //     })
            //     .style("fill","none")
            //     .style("stroke", "#fff")
            //     .style("stroke-width", "2px");
            // dom.global_rect
            //  .attr({
            //      "width": img_data.global.width,
            //         "height": img_data.global.height,
            //  });
            // dom.global_rect.exit().remove();

            dom.global_img = dom.global_g.selectAll("image").data([img_data])
            dom.global_img.enter()
                .append("image")
                .attr("xlink:href", function(d) {
                    return d[2];
                })
            dom.global_img.call(global_transform);
            dom.global_img.exit().remove();
            dom.global_brush = dom.global_g.selectAll("._brush").data(['only'])
            dom.global_brush.enter()
                .append("g")
                .attr("class", "_brush")
                .call(tool.brush)
            dom.global_brush.call(tool.brush)
            dom.global_brush.exit().remove();
            dom.global_brush.selectAll(".resize").remove();
            dom.global_brush.selectAll(".background").remove();

            // dom.global_g.call(tool.brush);
            // dom.global_g.selectAll("g").remove();
            // dom.global_g.selectAll(".background").remove();
            // dom.global_g.selectAll(".resize").remove();
            // dom.global_g.selectAll("image").style("stroke","#fff");
        }

        function global_transform() {
            this.attr({
                height: function(d) {
                    return d.global.height
                },
                width: function(d) {
                    return d.global.width
                }
            })
            dom.global_g.attr("transform", function() {
                return "translate(0," + (box[1] - img_data.global.height) + ")"
            })
        }

        function data_() {
            img_data.aspect_ratiol = img_data[0] / img_data[1];
            img_data.global = {};
            img_data.global.width = Map_width;
            img_data.global.height = img_data.global.width / img_data.aspect_ratiol;
            img_data.global.scale = img_data[0] / img_data.global.width;

            img_data.zoom = {}
            if (img_data.aspect_ratiol >= box.aspect_ratiol) {
                img_data.zoom.height = box[1];
                img_data.zoom.width = box[1] * img_data.aspect_ratiol;
                img_data.zoom.scale = img_data.zoom.height / img_data.global.height
            } else {
                img_data.zoom.width = box[0];
                img_data.zoom.height = box[0] / img_data.aspect_ratiol;
                img_data.zoom.scale = img_data.zoom.height / img_data.global.height
               
            }
            img_data.o = {
                width: img_data.zoom.width,
                height: img_data.zoom.height,
                scale: img_data.zoom.width / img_data.global.width
            };
            zoom_translate_( _.tagCoordinate)
            
        }

        function zoom_translate_(arr) {
            if (img_data.aspect_ratiol >= box.aspect_ratiol) {
                _.transform.o = {
                    "translate":{
                        "brush" : [[0,0],[box[0]/img_data.o.scale,box[1]/img_data.o.scale]],
                        "zoom" : [0,0]
                    },
                    "scale":1
                };
                _.transform.cu = _.transform.cu || _.transform.o
                _.transform.really = really_();
            _.transform.cu = _.transform.really;
                function really_() {
                    var abs_x = arr[0]/img_data.global.scale;
                    if (abs_x <= (_.transform.cu.translate.brush[1][0]-_.transform.cu.translate.brush[0][0]) / 2 ) {
                        return _.transform.o;
                    }else if (abs_x <= (img_data.global.width - (_.transform.cu.translate.brush[1][0] - _.transform.cu.translate.brush[0][0]) /2) ) {
                        return {
                            "translate":{
                                "brush":[[abs_x - (_.transform.cu.translate.brush[1][0] - _.transform.cu.translate.brush[0][0]) /2,0],[arr[0]/img_data.global.scale + (_.transform.cu.translate.brush[1][0] - _.transform.cu.translate.brush[0][0])/2,_.transform.cu.translate.brush[1][1]]],
                                "zoom":[-(abs_x - (_.transform.cu.translate.brush[1][0] - _.transform.cu.translate.brush[0][0]) /2) * img_data.o.scale,0]
                            },
                            "scale":1
                        }
                    }else{
                        return {
                            "translate":{
                                "brush":[[img_data.global.width - _.transform.cu.translate.brush[1][0],0],[img_data.global.width,_.transform.cu.translate.brush[1][1]]],
                                "zoom":[-(img_data.global.width - _.transform.cu.translate.brush[1][0]) * img_data.o.scale,0]
                            },
                            "scale":1
                        }
                    }
                }
            } else {
                 _.transform.o = {
                    "translate":{
                        "brush" : [[0,0],[box[0]/img_data.o.scale,box[1]/img_data.o.scale]],
                        "zoom" : [0,0]
                    },
                    "scale":1
                };
                _.transform.cu = _.transform.cu || _.transform.o
                _.transform.really = really_();
            _.transform.cu = _.transform.really;

                function really_() {
			var abs_y = arr[1]/img_data.global.scale;
                    if (abs_y <= (_.transform.cu.translate.brush[1][1]-_.transform.cu.translate.brush[0][1]) / 2 ) {
                        return _.transform.o;
                    }else if (abs_y <= (img_data.global.height - (_.transform.cu.translate.brush[1][1] - _.transform.cu.translate.brush[0][1]) /2) ) {
                        return {
                            "translate":{
                                "brush":[[0,abs_y - (_.transform.cu.translate.brush[1][1] - _.transform.cu.translate.brush[0][1])/2],[_.transform.cu.translate.brush[1][0],arr[1]/img_data.global.scale + (_.transform.cu.translate.brush[1][1] - _.transform.cu.translate.brush[0][1])/2]],
                                "zoom":[0,-(abs_y - (_.transform.cu.translate.brush[1][1] - _.transform.cu.translate.brush[0][1])/2)* img_data.o.scale]
                            },
                            "scale":1
                        }
                    }else{
                        return {
                            "translate":{
                                "brush":[[0,img_data.global.height - _.transform.cu.translate.brush[1][1]],[img_data.global.width,img_data.global.height]],
                                "zoom":[0,-(img_data.global.height - _.transform.cu.translate.brush[1][1]) * img_data.o.scale]
                            },
                            "scale":1
                        }
                    }
                }
            }
        }

        // setTimeout(function() {
            window.addEventListener("deviceorientation", orientationHandler, false);
        // }, 1000)

        function orientationHandler(event) {

            d3.selectAll('use')
                .style({
                    "transform": function(d) {
                        if (d3.select(this).attr("class") == "tag") {
                            d[2] = event.webkitCompassHeading || 180 - event.alpha;
                        } else {
                            d[2] = 0
                        }
                       

                        // var anzhuo = 180//event.webkitCompassHeading ? 180 : 0
                        // d[2] = d3.select(this).attr("class") == "tag" ?( event.webkitCompassHeading || 180 - event.alpha) : anzhuo;
                        // alert(d[2])
                        // return "translate(" + (d[0] - 10 * img_data.zoom.scale) + "px," + (d[1] - 18 * img_data.zoom.scale) + "px)" + "rotate(" + (d[2]) + "deg)"
                            // return "translate(-10px,-18px)"+"rotate(0deg)"
                            // "+(Math.random()*360)+"
                       var translate = "translate(" + (- 10 + d[0]  * (img_data.o.scale / img_data.global.scale)) + "px," + (- 18 + d[1]  * (img_data.o.scale / img_data.global.scale)) + "px)";
                        var rotate = "rotate(" + (d[2]) + "deg)";
                        var scale = "scale("+(1 / (img_data.zoom.scale / img_data.global.scale) )+")"
                        
                        return translate + rotate + scale;     
                    }
                })
        }
        return _;
    }
    this.zoom_map = zoom_map;
}()
